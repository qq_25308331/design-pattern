package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 适配器模式
 */
public class AdapterPattern {

    public static void main(String[] args) {
        SimpleEncrypt simpleEncrypt = new SimpleEncrypt();
        EncryptAdapter adapter = new EncryptAdapter(simpleEncrypt);
        String encrypt = adapter.encrypt("hello adapter");
        System.out.println(encrypt);
    }

    private interface Encrypt {
        String encrypt(String text);
    }

    private static class EncryptAdapter implements Encrypt{

        private static final int ENCRYPT_LENGTH = 15;

        private final SimpleEncrypt simpleEncrypt;

        private EncryptAdapter(SimpleEncrypt simpleEncrypt) {
            this.simpleEncrypt = simpleEncrypt;
        }

        @Override
        public String encrypt(String text) {
            return simpleEncrypt.encrypt(text,ENCRYPT_LENGTH);
        }

    }

    private static class SimpleEncrypt {
        public String encrypt(String text,int length) {
            return text + length;
        }
    }

}
