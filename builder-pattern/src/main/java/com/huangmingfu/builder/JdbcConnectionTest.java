package com.huangmingfu.builder;

/**
 * @Author: 黄名富
 * @Description: JdbcConnectorBuilder 测试
 */
public class JdbcConnectionTest {

    public static void main(String[] args) {
        JdbcConnectorBuilder builder = new JdbcConnectorBuilder();
        builder.setScheme("mysql")
                .setHost("localhost")
                .setPort("3307")
                .setDatabase("my-database")
                .setUsername("root")
                .setPassword("123456");
        JdbcConnector jdbcConnector = builder.build();
        JdbcConnector.JdbcPreparedStatement statement = jdbcConnector.getPreparedStatement("select * from student");
        System.out.println(statement.getResult());
    }

}
