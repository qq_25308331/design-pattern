package com.huangmingfu.builder;

import java.net.URI;

/**
 * @Author: 黄名富
 * @Description: jdbc 连接器
 */
public class JdbcConnector {

    private JdbcConnection connection;

    public void initConnection(URI uri) {
         connection = new JdbcConnection(uri);
    }

    public JdbcPreparedStatement getPreparedStatement(String query) {
        if (connection == null) throw new RuntimeException("连接器还未初始化");
        return connection.getPreparedStatement(query);
    }

    public static class JdbcConnection {
        public JdbcConnection(URI uri){
            System.out.println(uri);
        }

        public JdbcPreparedStatement getPreparedStatement(String query) {
            return new JdbcPreparedStatement(query);
        }
    }

    public static class JdbcPreparedStatement {
        String query;

        public JdbcPreparedStatement(String query) {
            this.query = query;
        }

        public String getResult() {
            return this.query;
        }
    }

}
