package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 访问者模式
 * @需求描述: 现在人们常用的电子产品有手机、平板及电脑。 不同的人对使用这些产品会有不同的用途
 */
public class VisitorPattern {

    public static void main(String[] args) {
        Element[] elements = {new MobilePhone(),new Ipad(),new Computer()};
        Visitor[] visitors = {new Student(),new Programmer()};
        for (Visitor visitor : visitors) {
            for (Element element : elements) {
                element.accept(visitor);
            }
        }
    }

    private interface Element {
        void accept(Visitor visitor);
    }

    private static class MobilePhone implements Element{

        @Override
        public void accept(Visitor visitor) {
            visitor.visit(this);
        }

        public void makeCall() {
            System.out.println("打电话");
        }

        public void wechat() {
            System.out.println("微信聊天");
        }

        public void playGame() {
            System.out.println("玩王者荣耀");
        }
    }

    private static class Ipad implements Element {

        @Override
        public void accept(Visitor visitor) {
            visitor.visit(this);
        }

        public void watchTv() {
            System.out.println("刷剧");
        }

        public void playGame() {
            System.out.println("玩王者荣耀,大屏更爽");
        }
    }

    private static class Computer implements Element {

        @Override
        public void accept(Visitor visitor) {
            visitor.visit(this);
        }

        public void playGame() {
            System.out.println("玩电脑游戏：使命召唤");
        }

        public void work() {
            System.out.println("用于工作");
        }
    }

    private interface Visitor {
        void visit(MobilePhone mobilePhone);
        void visit(Ipad ipad);
        void visit(Computer computer);
    }

    private static class Programmer implements Visitor {

        @Override
        public void visit(MobilePhone mobilePhone) {
            mobilePhone.makeCall();
        }

        @Override
        public void visit(Ipad ipad) {
            ipad.playGame();
        }

        @Override
        public void visit(Computer computer) {
            computer.work();
        }
    }

    private static class Student implements Visitor {

        @Override
        public void visit(MobilePhone mobilePhone) {
            mobilePhone.wechat();
        }

        @Override
        public void visit(Ipad ipad) {
            ipad.watchTv();
        }

        @Override
        public void visit(Computer computer) {
            computer.playGame();
        }
    }

}
