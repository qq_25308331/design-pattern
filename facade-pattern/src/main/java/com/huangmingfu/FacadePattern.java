package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 门面模式
 */
public class FacadePattern {

    public static void main(String[] args) { // 客户端
        SystemController controller = SystemController.getInstance();
        System.out.println(controller.getUser("facade"));
    }

    private static class SystemController { // 对外的接口

        private static volatile SystemController instance;

        public static SystemController getInstance() {
            if (instance == null) {
                synchronized (SystemController.class) {
                    if (instance == null) {
                        UserService service = new UserService(new UserMapper());
                        instance = new SystemController(service);
                    }
                }
            }
            return instance;
        }

        private final UserService userService;

        private SystemController(UserService userService) {
            this.userService = userService;
        }

        public String getUser(String username) {
            return userService.getUser(username);
        }
    }

    private static class UserService {
        private final UserMapper userMapper;

        private UserService(UserMapper userMapper) {
            this.userMapper = userMapper;
        }

        public String getUser(String username) {
            if (username == null) return "";
            return userMapper.searcherByUsername(username);
        }

    }

    private static class UserMapper {
        public String searcherByUsername(String username) {
            return "用户+" + username;
        }
    }

}
