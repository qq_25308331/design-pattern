package com.huangmingfu.substitution;

import java.util.Random;

/**
 * @Author: 黄名富
 * @Description: 重写父类方法（不符合里氏替换原则）
 * @CreationDate: 2024/4/25
 */
public class RewriteParentMethod {

    public static void main(String[] args) {
        Phone phone = new SmartPhone();
        phone.call();
        /*
          这里我们预期的是，如果手机卡没就绪就提示消息，否则就拨号成功。
          但是其子类SmartPhone 重写了这个方法，我们无法保证执行这个方法符合我们的预期要求。
         */
    }

    private static class Phone {
        public void call() {
            Random random = new Random();
            if (!random.nextBoolean()) {
                System.out.println("手机卡未就绪，不能拨打电话");
                return;
            }
            System.out.println("拨打电话成功");
        }
    }

    private static class SmartPhone extends Phone {
        @Override
        public void call() {
            Random random = new Random();
            if (random.nextBoolean()) {
                throw new RuntimeException("视频通话功能开启失败");
            }
            System.out.println("开启视频通话功能");
            super.call();
        }
    }

}
