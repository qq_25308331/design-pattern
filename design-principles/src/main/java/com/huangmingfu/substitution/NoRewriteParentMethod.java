package com.huangmingfu.substitution;

import java.util.Random;

/**
 * @Author: 黄名富
 * @Description:
 * @CreationDate: 2024/4/25
 */
public class NoRewriteParentMethod {

    public static void main(String[] args) {
        Phone phone = new SmartPhone();
        phone.call();
    }

    private static abstract class Phone {
        public abstract void beforeCall() throws Exception;

        public abstract void afterCall() throws Exception;

        public void call() {
            try {
                beforeCall();
                Random random = new Random();
                if (!random.nextBoolean()) {
                    System.out.println("手机卡未就绪，不能拨打电话");
                    return;
                }
                System.out.println("拨打电话成功");
                afterCall();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static class SmartPhone extends Phone {

        @Override
        public void beforeCall() throws Exception {
            Random random = new Random();
            if (random.nextBoolean()) {
                throw new Exception("视频通话功能开启失败");
            }
            System.out.println("开启视频通话功能");
        }

        @Override
        public void afterCall() throws Exception {
            System.out.println("关闭视频通话功能");
        }
    }


}
