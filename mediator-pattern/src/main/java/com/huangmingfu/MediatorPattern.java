package com.huangmingfu;

import java.util.*;

/**
 * @Author: 黄名富
 * @Description: 不使用中介者模式
 * @需求描述: 在前端开发中，有三个组件 Button、View, 及Text. View 用来展示信息，Text 用于输入编辑信息，Button用来提交更新。
 * 用户在Text输入好内容后，点击Button后，内容会更新到View. 而点击View时，会把内容输入到Text。
 */
public class MediatorPattern {

    public static void main(String[] args) {

        Mediator mediator = new ContentMediator();

        Component text = new Text(mediator, "text");
        Component button = new Button(mediator,"button");
        Component view = new View(mediator,"view");

        mediator.registry(text);
        mediator.registry(button);
        mediator.registry(view);

        button.onClick();
        button.onClick();
        view.onClick();
        button.onClick();
    }

    private static abstract class Mediator {

        protected final Set<Component> components = new HashSet<>();

        public void registry(Component component) {
            if (component != null) {
                components.add(component);
            }
        }

        abstract void update(String content,String target);

    }

    private static class ContentMediator extends Mediator{

        @Override
        public void update(String content,String target) {
            if (content == null) {
                Text text = getText();
                if (text == null) throw new RuntimeException("没有更新内容");
                content = text.getContent();
            }
            for (Component component : components) {
                if (component.getTag().equals(target)) {
                    component.onRefresh(content);
                }
            }
        }

        private Text getText() {
            for (Component component : components) {
                if ("text".equals(component.getTag())) return (Text) component;
            }
            return null;
        }
    }

    private static abstract class Component {
        protected final Mediator mediator;
        private final String tag;

        protected Component(Mediator mediator, String tag) {
            this.mediator = mediator;
            this.tag = tag;
        }

        public String getTag() {
            return tag;
        }

        abstract void onClick();

        abstract void onRefresh(String content);
    }

    private static class Text extends Component {

        private String content;

        protected Text(Mediator mediator, String tag) {
            super(mediator, tag);
        }

        @Override
        void onClick() { // 输入操作
            throw new RuntimeException("暂不支持Text的点击事件");
        }

        @Override
        void onRefresh(String content) {
            this.content = content;
        }

        public String getContent() {
            Random random = new Random();
            String temp = content;
            if (temp == null) temp = "";
            temp += random.nextInt() + "@";
            content = null;
            return temp;
        }
    }

    private static class View extends Component {

        private String content;

        protected View(Mediator mediator, String tag) {
            super(mediator, tag);
        }

        @Override
        void onClick() {
            mediator.update(content,"text");
        }

        @Override
        void onRefresh(String content) {
            this.content = content;
            System.out.println("view更新："+ content);
        }
    }

    private static class Button extends Component {

        protected Button(Mediator mediator, String tag) {
            super(mediator, tag);
        }

        @Override
        void onClick() {
            mediator.update(null,"view");
        }

        @Override
        void onRefresh(String content) {
            throw new RuntimeException("暂不支持Button的更新操作");
        }
    }

}
