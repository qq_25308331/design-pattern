package com.huangmingfu;

import java.util.*;

/**
 * @Author: 黄名富
 * @Description: 命令模式
 * @需求描述: 在餐厅中，有顾客、厨师这两种角色，顾客扫描下单，厨师根据订单做菜。
 */
public class CommandPattern {

    public static void main(String[] args) { // 进入餐馆
        String[] food_menu = {"麻婆豆腐","红烧茄子","莴笋炒肉","辣子鸡丁","香干炒肉","黄焖鸡"};
        Cook[] cooks = new Cook[4];
        for (int i = 0; i < cooks.length; i++) { // 招募厨子
            cooks[i] = new Cook();
        }
        for (int i = 0; i < 10; i++) { // 来了客户点单
            Customer order = new Customer(i);
            CookingCommand command = new CookingCommand(cooks);
            order.placeOrder(command,food_menu);
        }
    }


    private static class Customer {

        private final int id;

        public Customer(int id) {
            this.id = id;
        }

        void placeOrder(Command command, String[] foodNames) {
            Random random = new Random();
            int num = random.nextInt(foodNames.length) + 1;
            Set<String> foods = new HashSet<>();
            while (num > 0) {
                String foodName = foodNames[random.nextInt(foodNames.length)];
                if (foods.add(foodName)) num--;
            }
            command.makeOrder(foods,id);
        }
    }

    private interface Command {
        void makeOrder(Collection<String> foodList,int id);
    }

    private static class CookingCommand implements Command {

        private final Cook[] cookList;

        public CookingCommand(Cook[] cookList) {
            this.cookList = cookList;
        }

        @Override
        public void makeOrder(Collection<String> foodList,int id) {
            System.out.println("客户:" + id + ",下单：" + foodList);
            new Thread(() -> {
                Date date = new Date();
                List<String> finished = new ArrayList<>();
                for (String food : foodList) {
                    boolean cooking = true;
                    int pos = 0;
                    while (cooking) {
                        Cook cook = cookList[pos];
                        try {
                            String finishedFood = cook.cooking(food);
                            finished.add(finishedFood);
                            cooking = false;
                        } catch (Exception e) {
                            pos = (pos + 1) % cookList.length;
                        }
                    }
                }
                Date finishedDate = new Date();
                long distance = (finishedDate.getTime() - date.getTime()) / 1000 ;
                System.out.println("订单：" + id + "完成，耗时：" + distance + "秒," + finished);
            }).start();
        }
    }

    private static class Cook {

        private Boolean isBusy = false;

        private String cooking(String foodName) throws Exception {
            if (isBusy) {
                throw new Exception("没空！");
            }
            isBusy = true;
            System.out.println(foodName + "制作中...");
            synchronized (this) {
                Thread.sleep(2000);
            }
            isBusy = false;
            return "f-" + foodName;
        }
    }

}
