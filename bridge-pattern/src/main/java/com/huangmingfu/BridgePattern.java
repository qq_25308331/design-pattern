package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 桥接模式
 */
public class BridgePattern {

    public static void main(String[] args) {
        TasteImplementor[] tasteArray = {new SugarTasteImplementor(),new SugarTasteImplementor()};
        for (int i = 0; i < tasteArray.length; i++) {
            for (int j = 0; j < 2; j++) {
                CoffeeAbstraction coffee = i == 0 ? new BigCoffee(tasteArray[i]) : new SmallCoffee(tasteArray[i]);
                coffee.order();
            }
        }

    }

    private static abstract class CoffeeAbstraction {

        protected TasteImplementor tasteImplementor;

        public CoffeeAbstraction(TasteImplementor tasteImplementor) {
            this.tasteImplementor = tasteImplementor;
        }

        abstract String size();

        void order() {
            System.out.println(size() + tasteImplementor.taste() + "咖啡");
        }
    }

    private static class BigCoffee extends CoffeeAbstraction{

        public BigCoffee(TasteImplementor tasteImplementor) {
            super(tasteImplementor);
        }

        @Override
        String size() {
            return "大杯";
        }
    }

    private static class SmallCoffee extends CoffeeAbstraction {

        public SmallCoffee(TasteImplementor tasteImplementor) {
            super(tasteImplementor);
        }

        @Override
        String size() {
            return "小杯";
        }
    }

    private interface TasteImplementor {
        String taste();
    }

    private static class SugarTasteImplementor implements TasteImplementor{

        @Override
        public String taste() {
            return "加糖";
        }
    }

    private static class NoSugarTasteImplementor implements TasteImplementor{

        @Override
        public String taste() {
            return "不加糖";
        }
    }

}
