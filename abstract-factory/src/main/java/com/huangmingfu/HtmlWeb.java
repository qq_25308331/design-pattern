package com.huangmingfu;

import com.huangmingfu.factory.ComponentsFactory;
import com.huangmingfu.factory.DarkComponentFactory;
import com.huangmingfu.factory.LightComponentFactory;

/**
 * @Author: 黄名富
 * @Description: 页面
 */
public class HtmlWeb {

    public static void main(String[] args) {
        ComponentsFactory componentsFactory = new DarkComponentFactory();
        System.out.println("-----------dark模式-----------");
        System.out.println(componentsFactory.buildButton());
        System.out.println(componentsFactory.buildText());
        System.out.println("-----------light模式-----------");
        componentsFactory = new LightComponentFactory();
        System.out.println(componentsFactory.buildButton());
        System.out.println(componentsFactory.buildText());
    }

}
