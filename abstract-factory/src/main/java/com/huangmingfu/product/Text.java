package com.huangmingfu.product;

/**
 * @Author: 黄名富
 * @Description: text 组件
 */
public class Text {

    private final String color;

    public Text(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "color='" + color + '\'' +
                '}';
    }
}
