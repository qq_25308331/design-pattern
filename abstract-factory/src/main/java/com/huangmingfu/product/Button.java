package com.huangmingfu.product;

/**
 * @Author: 黄名富
 * @Description: button组件
 */
public class Button {

    private final String color;

    public Button(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "color='" + color + '\'' +
                '}';
    }
}
