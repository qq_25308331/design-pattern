package com.huangmingfu.factory;

import com.huangmingfu.product.Button;
import com.huangmingfu.product.LightButton;
import com.huangmingfu.product.LightText;
import com.huangmingfu.product.Text;

/**
 * @Author: 黄名富
 * @Description: light模式下的组件工厂
 */
public class LightComponentFactory implements ComponentsFactory{

    private final String color = "light";

    @Override
    public Button buildButton() {
        return new LightButton(color);
    }

    @Override
    public Text buildText() {
        return new LightText(color);
    }
}
