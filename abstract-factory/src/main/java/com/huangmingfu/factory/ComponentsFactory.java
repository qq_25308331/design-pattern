package com.huangmingfu.factory;

import com.huangmingfu.product.Button;
import com.huangmingfu.product.Text;

/**
 * @Author: 黄名富
 * @Description: 组件工厂
 */
public interface ComponentsFactory {

    Button buildButton();

    Text buildText();

}
