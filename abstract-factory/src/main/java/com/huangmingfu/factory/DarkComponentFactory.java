package com.huangmingfu.factory;

import com.huangmingfu.product.Button;
import com.huangmingfu.product.DarkButton;
import com.huangmingfu.product.DarkText;
import com.huangmingfu.product.Text;

/**
 * @Author: 黄名富
 * @Description: dark模式下的组件工厂
 */
public class DarkComponentFactory implements ComponentsFactory{

    private final String color = "dark";

    @Override
    public Button buildButton() {
        return new DarkButton(color);
    }

    @Override
    public Text buildText() {
        return new DarkText(color);
    }

}
