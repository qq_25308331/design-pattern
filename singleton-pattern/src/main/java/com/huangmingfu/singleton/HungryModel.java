package com.huangmingfu.singleton;

/**
 * @Author: 黄名富
 * @Description: 饿汉模式
 * @CreationDate: 2024/4/29
 */
public class HungryModel {

    private HungryModel() {}

    private final static HungryModel instance = new HungryModel();

    public static HungryModel getInstance() {
        return instance;
    }
}
