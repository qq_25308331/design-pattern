package com.huangmingfu.singleton;

/**
 * @Author: 黄名富
 * @Description: IoDH(Initialization-on-Demand Holder) 单例模式
 * @CreationDate: 2024/4/29
 */
public class IoDHSingleton {

    private IoDHSingleton() {}

    private static class Holder {
        static final IoDHSingleton instance = new IoDHSingleton();
    }

    public static IoDHSingleton getInstance() {
        return Holder.instance;
    }

}
