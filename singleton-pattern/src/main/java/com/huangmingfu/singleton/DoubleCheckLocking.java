package com.huangmingfu.singleton;

/**
 * @Author: 黄名富
 * @Description: 双重检查锁定
 * @CreationDate: 2024/4/29
 */
public class DoubleCheckLocking {

    private DoubleCheckLocking() {}

    private static DoubleCheckLocking instance = null;

    public static DoubleCheckLocking getInstance() {
        if (instance == null) {
            synchronized (DoubleCheckLocking.class) {
                if (instance == null) instance = new DoubleCheckLocking();
            }
        }
        return instance;
    }
}
