package com.huangmingfu.prototype.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: 黄名富
 * @Description: 用户实体
 * @CreationDate: 2024/4/29
 */
public class User implements Cloneable{

    private List<Address> addressList;

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    @Override
    public User clone() throws CloneNotSupportedException {
        User clone = (User)super.clone();
        if (addressList != null) {
            List<Address> newList = new ArrayList<>();
            for (Address address : addressList) {
                newList.add(address.clone());
            }
            clone.setAddressList(newList);
        }
        return clone;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        User user = new User();
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address());
        addresses.add(new Address());
        user.setAddressList(addresses);

        User clone = user.clone();
        System.out.println(user);
        System.out.println(clone);
    }

    @Override
    public String toString() {
        return "User(" + Integer.toHexString(hashCode()) +"){" +
                "addressList=" + addressList +
                '}';
    }
}
