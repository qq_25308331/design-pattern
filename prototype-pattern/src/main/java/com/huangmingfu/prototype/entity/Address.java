package com.huangmingfu.prototype.entity;

/**
 * @Author: 黄名富
 * @Description: 地址实体
 * @CreationDate: 2024/4/29
 */
public class Address implements Cloneable{

    public Address() {}

    @Override
    public Address clone() throws CloneNotSupportedException {
        return (Address)super.clone();
    }

    @Override
    public String toString() {
        return "Address(" + Integer.toHexString(hashCode()) + ")";
    }
}
