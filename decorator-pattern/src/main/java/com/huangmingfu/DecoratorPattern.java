package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 装饰模式
 */
public class DecoratorPattern {

    public static void main(String[] args) {
        String text = "hello decorator";
        Verifier noEmptyVerifier = new NoEmptyVerifier();
        Verifier minLengthVerifier = new MinLengthVerifier(noEmptyVerifier,7);
        System.out.println(minLengthVerifier.verification(text));
    }

    private interface Verifier {
        boolean verification(String text);
    }

    private static class NoEmptyVerifier implements Verifier {

        @Override
        public boolean verification(String text) {
            return text != null && text.trim().length() > 0;
        }
    }

    private static class MinLengthVerifier implements Verifier {

        private final int DEFAULT_MIN_LENGTH = 15;

        private final Verifier verifier;
        private int minLength = DEFAULT_MIN_LENGTH;

        private MinLengthVerifier(Verifier verifier) {
            this.verifier = verifier;
        }

        public MinLengthVerifier(Verifier verifier, int minLength) {
            this.verifier = verifier;
            this.minLength = minLength;
        }

        @Override
        public boolean verification(String text) {
            return verifier.verification(text) && text.length() > minLength;
        }
    }

}
