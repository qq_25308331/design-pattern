package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 模板方法模式
 */
public class TemplateMethodPattern {

    public static void main(String[] args) {
        Worker programmer = new Programmer();
        programmer.work();
    }

    private static abstract class Worker {

        public void work() {
            punch("上班");
            duty();
            punch("下班");
        }

        protected abstract void duty();

        protected void punch(String content) {
            System.out.println("打卡:" + content);
        }
    }

    private static class Programmer extends Worker {

        @Override
        protected void duty() {
            System.out.println("写bug AND 解决bug");
        }
    }

}
