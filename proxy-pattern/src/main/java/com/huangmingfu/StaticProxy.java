package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 静态代理
 * @CreationDate: 2024/5/21
 */
public class StaticProxy {

    public static void main(String[] args) {
        Subject request = new SimpleRequest();
        Subject proxy = new LogProxy(request);
        proxy.request("applet","123456");
    }

    private interface Subject {
        void request(String source,String token);
    }

    private static class LogProxy implements Subject {

        private final Subject subject;

        private LogProxy(Subject subject) {
            this.subject = subject;
        }

        @Override
        public void request(String source, String token) {
            System.out.println("记录访问日志,source=" + source + ",token=" + token);
            subject.request(source,token);
        }
    }

    private static class SimpleRequest implements Subject {
        @Override
        public void request(String source, String token) {
            System.out.println("接收请求:" + source + "," + token);
        }
    }

}
