package com.huangmingfu;

/**
 * @Author: 黄名富
 * @Description: 虚拟代理
 */
public class VirtualAgent {

    private interface Image {
        void display();
    }

    private static class RealImage implements Image {

        private String path;

        public RealImage(String path) {
            System.out.println("创建一个图片需要许多资源，耗费时间较长");
            this.path = path;
        }

        @Override
        public void display() {
            System.out.println("展示图片:" + path);
        }
    }

    private static class ProxyImage implements Image {

        private volatile Image image;
        private String path;

        public ProxyImage(String path) {
            System.out.println("虚拟对象，需要资源及时间都很少，用于延迟真实对象的创建与访问");
            this.path = path;
        }

        @Override
        public void display() {
            if (image == null) {
                synchronized (ProxyImage.class) {
                    if (image == null) image = new RealImage(path);
                }
            }
            image.display();
        }
    }

    public static void main(String[] args) {
        Image[] images = new Image[10];
        // 在网页加载过程，先加载这些图片，为了防止陷入卡顿，不直接创建图片，而是先创建虚拟对象
        System.out.println("网页加载中...");
        for (int i = 0; i < images.length; i++) {
            images[i] = new ProxyImage(i + ".png");
        }
        System.out.println("网页加载完成--------");
        // 当网页加载完成后，可以依次加载真实图片了
        for (Image image : images) {
            image.display();
        }
    }

}
