package com.huangmingfu;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @Author: 黄名富
 * @Description: 远程服务
 */
public class RemoteServiceImpl extends UnicastRemoteObject implements RemoteService{

    public RemoteServiceImpl() throws RemoteException{
    }

    @Override
    public String service(String requestInfo) throws RemoteException {
        System.out.println("请求：" + requestInfo);
        return "RemoteService提供远程服务：" + requestInfo;
    }
}
