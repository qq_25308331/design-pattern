package com.huangmingfu;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * @Author: 黄名富
 * @Description:
 */
public class JNDIPublisher {

    public static void main(String[] args) throws RemoteException, MalformedURLException {
        RemoteService remoteService = new RemoteServiceImpl();
        int port = 8080;
        String serviceName = "rmi://localhost:" + port + "/remoteService";
        LocateRegistry.createRegistry(port);
        Naming.rebind(serviceName,remoteService);
    }

}
