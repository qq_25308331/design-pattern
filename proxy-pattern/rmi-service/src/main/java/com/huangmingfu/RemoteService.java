package com.huangmingfu;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @Author: 黄名富
 * @Description: 远程服务
 */
public interface RemoteService extends Remote {

    String service(String requestInfo) throws RemoteException;

}
