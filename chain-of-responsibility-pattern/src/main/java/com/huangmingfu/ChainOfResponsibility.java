package com.huangmingfu;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: 黄名富
 * @Description: 职责链模式
 * @需求描述: 当客户端向服务器发起一个请求时，服务器将对这个请求做一系列校验。 只有这些校验通过后才能方向请求
 */
public class ChainOfResponsibility {

    public static void main(String[] args) {
        RequestHandler requestHandler = generateChain();
        List<CustomRequest> requestList = generateRequest();
        for (CustomRequest request : requestList) {
            try {
                requestHandler.handle(request);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private static RequestHandler generateChain() {
        RequestHandler handler = new SubjectHandler();
        RequestHandler usernameHandler = new UserNameHandler(handler);
        return new IpRequestHandler(usernameHandler);
    }

    private static List<CustomRequest> generateRequest() {
        List<CustomRequest> list = new ArrayList<>();
        list.add(new CustomRequest("localhost", "user"));
        list.add(new CustomRequest("172.34.43.32", "admin"));
        list.add(new CustomRequest("172.34.24.24", "user"));
        return list;
    }

    private static class CustomRequest {
        String ip;
        String username;

        public CustomRequest(String ip, String username) {
            this.ip = ip;
            this.username = username;
        }

        @Override
        public String toString() {
            return "CustomRequest{" +
                    "ip='" + ip + '\'' +
                    ", username='" + username + '\'' +
                    '}';
        }
    }

    private static abstract class RequestHandler {

        protected final RequestHandler handler;

        public RequestHandler(RequestHandler handler) {
            this.handler = handler;
        }

        abstract void handle(CustomRequest request);
    }

    private static class IpRequestHandler extends RequestHandler{

        private static final String[] BLACK_IPS = {"localhost","127.0.0.1"};

        public IpRequestHandler(RequestHandler handler) {
            super(handler);
        }

        @Override
        void handle(CustomRequest request) {
            if (request.ip == null) {
                throw new RuntimeException("请求IP 不合法");
            } else {
                for (String str : BLACK_IPS) {
                    if (request.ip.contains(str)) throw new RuntimeException("请求IP 不合法");
                }
            }
            handler.handle(request);
        }
    }

    private static class UserNameHandler extends RequestHandler {

        public UserNameHandler(RequestHandler handler) {
            super(handler);
        }

        @Override
        void handle(CustomRequest request) {
            if (request.username == null) {
                throw new RuntimeException("用户名不能为空");
            } else {
                if (request.username.contains("admin")) throw new RuntimeException("用户名不合法");
            }
            handler.handle(request);
        }
    }

    private static class SubjectHandler extends RequestHandler {

        public SubjectHandler() {
            super(null);
        }

        @Override
        void handle(CustomRequest request) {
            System.out.println("请求到达目标处理器：" + request);
        }
    }

}
