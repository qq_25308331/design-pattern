package com.huangmingfu.factory.factory;

import com.huangmingfu.factory.product.Car;
import com.huangmingfu.factory.product.TeslaCar;

/**
 * @Author: 黄名富
 * @Description:
 */
public class TeslaCarFactory implements CarFactory{
    @Override
    public Car buildCar() {
        return new TeslaCar("电动发动机");
    }
}
