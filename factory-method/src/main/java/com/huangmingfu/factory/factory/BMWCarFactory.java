package com.huangmingfu.factory.factory;

import com.huangmingfu.factory.product.BMWCar;
import com.huangmingfu.factory.product.Car;

/**
 * @Author: 黄名富
 * @Description: 宝马汽车工厂
 */
public class BMWCarFactory implements CarFactory{
    @Override
    public Car buildCar() {
        return new BMWCar("汽油发动机");
    }
}
