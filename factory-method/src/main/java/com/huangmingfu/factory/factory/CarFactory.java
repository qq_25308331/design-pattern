package com.huangmingfu.factory.factory;

import com.huangmingfu.factory.product.Car;

/**
 * @Author: 黄名富
 * @Description: 汽车工厂
 */
public interface CarFactory {

    Car buildCar();

}
