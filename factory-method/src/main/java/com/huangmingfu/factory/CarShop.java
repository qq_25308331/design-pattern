package com.huangmingfu.factory;

import com.huangmingfu.factory.factory.BMWCarFactory;
import com.huangmingfu.factory.factory.CarFactory;
import com.huangmingfu.factory.factory.TeslaCarFactory;

/**
 * @Author: 黄名富
 * @Description: 汽车店
 */
public class CarShop {

    public static void main(String[] args) {
        CarFactory carFactory = new BMWCarFactory();
        carFactory.buildCar();

        carFactory = new TeslaCarFactory();
        carFactory.buildCar();
    }

}
