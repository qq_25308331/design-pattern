package com.huangmingfu.factory.product;

/**
 * @Author: 黄名富
 * @Description: 特斯拉电动汽车
 */
public class TeslaCar extends Car{
    public TeslaCar(String engine) {
        super(engine);
    }
}
