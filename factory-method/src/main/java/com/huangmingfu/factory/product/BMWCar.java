package com.huangmingfu.factory.product;

/**
 * @Author: 黄名富
 * @Description: 宝马油车
 */
public class BMWCar extends Car{
    public BMWCar(String engine) {
        super(engine);
    }
}
