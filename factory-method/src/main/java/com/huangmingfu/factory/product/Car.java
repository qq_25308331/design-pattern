package com.huangmingfu.factory.product;

/**
 * @Author: 黄名富
 * @Description: 汽车
 */
public class Car {

    private String engine;

    protected Car(String engine) {
        this.engine = engine;
    }
}
